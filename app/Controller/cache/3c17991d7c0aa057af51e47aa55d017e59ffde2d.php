<?php $__env->startSection('content'); ?>
   <div class="table-responsive">
        <table class="table table-bordered" style="margin-top: 10%;">
            <tr class="info">
                <td class="text-center"><h4>Username</h4></td>
                <td class="text-center"><h4>Image</h4></td>
                <td class="text-center"><h4>Article Caption</h4></td>
            </tr>
            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr class="active">
                <td class="text-center valign"><?php echo e($user['username']); ?></td>
                <td class="text-center valign"><img src="images/<?php echo e($user['image']); ?>" width="100" height="100" /></td>
                <td class="text-center">
                    <div class="list-group">
                    <?php $__currentLoopData = $user['articles']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <button value="<?php echo e($article['id']); ?>" class="list-group-item btn btn-success article"><?php echo e($article['caption']); ?></button><br/>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </table>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>