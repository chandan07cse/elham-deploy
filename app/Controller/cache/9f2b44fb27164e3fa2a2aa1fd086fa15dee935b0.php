
<div class="confirm"></div>
<form action="#" method="POST"  role="form" id="articleInputForm">
    <div class="form-group col-sm-12 <?php echo e(@$errors->caption ? 'has-error' : ''); ?>">
        <label for="caption">Caption</label>
        <input class="form-control" name="caption" type="text" <?php echo e(@$errors->caption ? 'autofocus' : ''); ?> value="<?php echo e(@$oldValue->caption); ?>" id="caption">
        <div class="captionErrors"></div>
    </div>
    <div class="form-group col-sm-12 <?php echo e(@$errors->description ? 'has-error' : ''); ?>">
        <label for="description">Description</label>
        
        <textarea name="description" class="summernote" id="description" title="Contents"></textarea>
        <div class="descriptionErrors"></div>
    </div>

    <div class="form-group col-sm-12">
        <button class="btn btn-primary">Input</button>
    </div>
</form>

