<?php $__env->startSection('content'); ?>
        <div class="container">
        <h4> Welcome <?php echo e(\Elham\Controller\AuthController::userName()); ?></h4><p></p>
                <legend>Your Articles</legend>
                <?php echo e(\Elham\Controller\BaseController::getFlash('deleteMsg')); ?>

                <ul class="list-group">
                <?php $__currentLoopData = $users->articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <li class="list-group-item"><a href="/articles/<?php echo e($article->id); ?>" class="list-group-item list-group-item-action list-group-item-info left"><?php echo e($article->caption); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </ul>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.dashboardMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>