<?php
    $errors = \Elham\Controller\BaseController::getWith('errorBag');
    $oldValue = \Elham\Controller\BaseController::getWith('oldInputs');
?>
<?php echo e(\Elham\Controller\BaseController::getFlash('userUpdateMessage')); ?>

<form action="/user/<?php echo e($userData['id']); ?>/update" method="POST"  role="form" enctype="multipart/form-data">
    <div class="form-group col-sm-12 <?php echo e(@$errors->username ? 'has-error' : ''); ?>">
        <label for="username">Username</label>
        <input class="form-control" name="username" type="text" <?php echo e(@$errors->username ? 'autofocus' : ''); ?> value="<?php echo e(@$userData['username']); ?>">
        <?php if(@$errors->username): ?>
            <ul>
                <?php $__currentLoopData = $errors->username; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="form-group col-sm-12 <?php echo e(@$errors->email ? 'has-error' : ''); ?>">
        <label for="email">Email</label>
        <input class="form-control" name="email" type="email" <?php echo e(@$errors->email ? 'autofocus':''); ?> value="<?php echo e(@$userData['email']); ?>">
        <?php if(@$errors->email): ?>
            <ul>
                <?php $__currentLoopData = $errors->email; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="form-group col-sm-12 <?php echo e(@$errors->password ? 'has-error' : ''); ?>">
        <label for="password">Password</label>
        <input class="form-control" name="password" <?php echo e(@$errors->password ? 'autofocus':''); ?> type="password">
        <?php if(@$errors->password): ?>
            <ul class="validate_error">
                <?php $__currentLoopData = $errors->password; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="form-group col-sm-12 <?php echo e(@$errors->confirm_password ? 'has-error' : ''); ?>">
        <label for="password_confirmation">Confirm Password</label>
        <input class="form-control" name="confirm_password" <?php echo e(@$errors->confirm_password ? 'autofocus':''); ?> type="password">
        <?php if(@$errors->confirm_password): ?>
            <ul class="validate_error">
                <?php $__currentLoopData = $errors->confirm_password; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="form-group col-sm-12 <?php echo e(@$errors->image ? 'has-error' : ''); ?>">
        <img src="images/<?php echo e($userData['image']); ?>" width="100" height="100" alt="" id="oldImage">
        <img id="currentImage">
        <input class="form-control" name="image"  <?php echo e(@$errors->image ? 'autofocus':''); ?> type="file"   />
        <input type="hidden" name="oldImageName" value="<?php echo e(@$userData['image']); ?>">
        <?php if(@$errors->image): ?>
            <ul class="validate_error">
                <?php $__currentLoopData = $errors->image; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="form-group col-sm-12">
        <button class="btn btn-primary">Update</button>
    </div>
</form>

