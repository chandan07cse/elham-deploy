<?php

use Phinx\Seed\AbstractSeed;

class TaskSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 4; $i++) {
            $data[] = [

                'user_id'      => $faker->uuid,
                'taskname'      => $faker->word,
                'taskdescription'      => $faker->address
                      ];
        }

        $this->insert('tasks', $data);
    }
}
