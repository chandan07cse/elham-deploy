<?php

use Phinx\Migration\AbstractMigration;

class Task extends AbstractMigration
{
    public function up()
    {
        $articles = $this->table('tasks');
        $articles->addColumn('user_id','integer')
            ->addForeignKey('user_id','users','id',['delete'=>'CASCADE','update'=>'CASCADE'])
            ->addColumn('taskname','string')
            ->addColumn('taskdescription','string')
            ->create();
    }

    public function down()
    {
        $this->dropTable('tasks');
    }
}
